using Microsoft.AspNetCore.Mvc;
using Sample.Models;
using Sample.WebApi.Extensions;

namespace Sample.WebApi.Controllers;

[Route("[controller]")]
public class SamplesController
    : ControllerBase
{
    [HttpGet]
    public IActionResult Get([FromQuery]QueryModel queryModel)
    {
        if (!queryModel.Status.HasValue)
        {
            return BadRequest("Problem in deserialization");
        }
        
        return Ok(queryModel.Status.Value.GetEnumDisplayName());
    }
}