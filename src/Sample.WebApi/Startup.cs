﻿using Sample.WebApi.Extensions;

namespace Sample.WebApi;

public class Startup
{
    public Startup(IConfiguration configuration)
    {
    }
    
    public void ConfigureServices(IServiceCollection services)
    {
        services
            .AddOpenApi()
            .AddControllers();
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }

        app.UseRouting();
        app.UseOpenApi();
        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });
    }
}