using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Sample.WebApi.Extensions;

public static class EnumExtensions
{
    public static string GetEnumDisplayName<T>(this T enumType) 
        where T : struct, IConvertible
    {
        var fieldInfo = enumType.GetType().GetField(enumType.ToString() ?? throw new InvalidOperationException());
        var enumMemberAttributes =
            (EnumMemberAttribute[])fieldInfo!
                .GetCustomAttributes(typeof(EnumMemberAttribute), false);
        if (enumMemberAttributes.Length > 0)
        {
            return enumMemberAttributes[0].Value ?? throw new InvalidOperationException();
        }
        
        var displayAttributes = 
            (DisplayAttribute[])fieldInfo!
                .GetCustomAttributes(typeof(DisplayAttribute), false);
        if (displayAttributes.Length > 0)
        {
            return displayAttributes[0].Name ?? throw new InvalidOperationException();
        }
        return enumType.ToString() ?? throw new InvalidOperationException();
    }
}