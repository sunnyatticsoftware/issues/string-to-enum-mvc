using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Extensions;
using Microsoft.OpenApi.Models;
using Sample.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Sample.WebApi.Extensions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddOpenApi(this IServiceCollection services)
    {
        var mainAssemblyName = typeof(Startup).Assembly.GetName().Name;
        var applicationAssemblyName = typeof(QueryModel).Assembly.GetName().Name;
            
        services.AddSwaggerGen(
            c =>
            {
                c.SwaggerDoc(
                    "v1",
                    new OpenApiInfo
                    {
                        Title = mainAssemblyName,
                        Version = "v1",
                        Description = "Sample",
                        Contact = new OpenApiContact
                        {
                            Name = "Diego",
                            Email = "diego.martin@sunnyatticsoftware.com"
                        }
                    });
                
                var xmlCommentsWebApi = Path.Combine(AppContext.BaseDirectory, $"{mainAssemblyName}.xml");
                c.IncludeXmlComments(xmlCommentsWebApi);
                var xmlCommentsApplication = Path.Combine(AppContext.BaseDirectory, $"{applicationAssemblyName}.xml");
                c.IncludeXmlComments(xmlCommentsApplication);

                AddEnumAsStringValues<Status>(c);
            });
            
        return services;
    }
    
    private static void AddEnumAsStringValues<TEnum>(SwaggerGenOptions swaggerGenOptions)
        where TEnum : struct, Enum
    {
        var values = Enum.GetValues<TEnum>();
        var parameters = 
            values
                .Select(x => new OpenApiString(x.GetEnumDisplayName()))
                .Cast<IOpenApiAny>().ToList();
                    
        swaggerGenOptions.MapType<TEnum>(
            () =>
                new OpenApiSchema
                {
                    Type = "string",
                    Enum = parameters,
                    Nullable = true
                });
    }
}