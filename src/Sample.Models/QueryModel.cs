namespace Sample.Models;

public class QueryModel
{
    /// <summary>
    /// The foo parameter
    /// </summary>
    /// <example>bar</example>
    public string? Foo { get; init; } = null;
    
    /// <summary>
    /// The status
    /// </summary>
    /// <example>on-hold</example>
    public Status? Status { get; set; } = null;
}