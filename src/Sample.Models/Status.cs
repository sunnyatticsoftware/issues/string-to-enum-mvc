using System.Runtime.Serialization;

namespace Sample.Models;

public enum Status
{
    [EnumMember(Value = "open")]
    Open,
    
    [EnumMember(Value = "on-hold")]
    OnHold
}