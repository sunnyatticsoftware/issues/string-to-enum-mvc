# string-to-enum-mvc

This is a simple MVC project with an endpoint that deserializes `[FromBody]` into a model that contains an enum.

Original issue: https://stackoverflow.com/q/72886747/2948212

## How to reproduce
1. Clone the repository
2. Run with `dotnet run --project ./src/Sample.WebApi/Sample.WebApi.csproj`
3. Open `http://localhost:5000/swagger` and send http requests through the UI with different status query parameters

## The goal
The goal is to be able to send requests such as
```
http://localhost:5000/Samples?Foo=bar&Status=on-hold 
```

and have the controller's action deserialize the `QueryModel` with the proper `Status.OnHold` value by using its `EnumMember`